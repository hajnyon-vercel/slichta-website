# Contributing

You want to help with development or just your favorite restaurant? Great. Let's go.

## Development

This project consist of two parts: this repository [slichta-website](https://gitlab.com/hajnyon-projects/slichta/slichta-website) which is used for building the website and [slichta-scraper](https://gitlab.com/hajnyon-projects/slichta/slichta-scraper) which is used for obtaining the data. Do you want to fix or improve something on the website or help rewriting the scraper to typescript? Great! Let's chat in issues of individual projects.

## Adding restaurant

To add restaurant's menus it is important to be able to read the data by machine - aka scrape them. To be able to do that, the data should be publicly available in some machine readable form, eg. website or public pdf. It is trickier to scrape data off of facebook (or other similar sites) as additional resources are needed (proxies, captcha solvers etc.).

If restaurant provides appropriate resource we can simply scrape it using [slichta-scraper](https://gitlab.com/hajnyon-projects/slichta/slichta-scraper). To add your restaurant, simply clone the repository, create branch and add your scraper. Start by adding some data about the site to [`src/restaurants.js`](https://gitlab.com/hajnyon-projects/slichta/slichta-scraper/-/blob/main/src/restaurants.js) file and continue by writing the `pageFunction`. You can use a template in the [`src/menus`](https://gitlab.com/hajnyon-projects/slichta/slichta-scraper/-/tree/main/src/menus) folder or take inspiration from other scrapers. Once finished create a merge request and assign me as a reviewer so I can check if everything is ready. If so I will add the code and update [Apify task](https://apify.com/) which triggers the automatic scraper and website build.
