module.exports = {
	purge: ["./src/app/**/*.{js,ts,jsx,tsx}", "./src/components/**/*.{js,ts,jsx,tsx}"],
	darkMode: "class",
	theme: {
		extend: {
			colors: {
				yellow: "#fbe748",
			},
		},
	},
	variants: {
		extend: {},
	},
	plugins: [],
};
