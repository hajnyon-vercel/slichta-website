# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.5.1] - 2024-11-28

### Fixed

-   updated links to repository
-   updated dependencies

## [1.5.0] - 2024-06-29

### Changed

-   migrated to next@14 and app router
-   migrated to tailwind@3
-   updated dependencies

### Removed

-   app info API endpoint

## [1.4.0] - 2024-05-12

### Added

-   meta tags and favicons
-   app's build info

### Changed

-   new handcrafted logo
-   repository migrated to a GitLab group
-   deployment migrated to Vercel CLI
-   updated dependencies

## [1.3.0] - 2021-11-15

### Added

-   changelog section in docs
-   last and next update info

## [1.2.0] - 2021-11-03

### Added

-   dark mode
-   docs page
-   API

## [1.0.0] - 2021-10-25

### Added

-   initial website release
