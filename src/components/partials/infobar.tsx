import { IScheduleInfo } from "../../services";
import { CountDown } from "../common/date/count-down";
import { UTCToLocal } from "../common/date/utc-to-local";

interface IInfobarProps {
	data: IScheduleInfo["data"];
}

export function Infobar({ data }: IInfobarProps) {
	return (
		<div className="bg-yellow text-gray-900 p-2 rounded-lg flex justify-between">
			<div>
				⌛ Last update: <UTCToLocal dateUTC={data.lastRunAt} className="font-bold" />
			</div>
			<div>
				⏳ Next update in:{" "}
				<CountDown
					to={data.nextRunAt}
					afterMessage="running (usually ~5 minutes)"
					className="font-bold"
				/>
			</div>
		</div>
	);
}
