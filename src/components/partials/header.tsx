import Image from "next/image";
import Link from "next/link";

import { Navbar } from ".";
import { Container } from "..";

export function Header() {
	return (
		<header className="grid-container bg-yellow dark:text-black">
			<Container className={"flex justify-between"}>
				<div className="flex items-center">
					<Link href="/" className="w-16 sm:w-32 md:w-full mr-4">
						<Image
							src="/logo.png"
							alt="Šlichta - meals menus"
							width={120}
							height={120}
							style={{
								maxWidth: "100%",
								height: "auto",
							}}
							priority
						/>
					</Link>
					<Link href="/" className="hover:underline">
						<h1 className="text-2xl md:text-6xl leading-tight font-medium">Šlichta</h1>
					</Link>
				</div>
				<Navbar />
			</Container>
		</header>
	);
}
