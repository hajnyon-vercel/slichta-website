"use client";
import Link from "next/link";
import { useState } from "react";

import { DarkModeButton } from ".";

const menuLinks = [
	{ path: "about", label: "About" },
	{ path: "docs", label: "Docs" },
];

export function Navbar() {
	const [navbarOpen, setNavbarOpen] = useState(false);
	return (
		<nav className="relative flex flex-wrap items-center justify-between px-2 py-2 bg-skr-blue">
			<div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
				<div className="w-full relative flex lg:static lg:block justify-end">
					<button
						className="text-white cursor-pointer text-4xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block md:hidden outline-none focus:outline-none"
						type="button"
						onClick={() => setNavbarOpen(!navbarOpen)}
					>
						☰
					</button>
				</div>
				<div
					className={"md:flex grow items-center" + (navbarOpen ? " flex" : " hidden")}
					id="example-navbar-danger"
				>
					<ul className="flex flex-col md:flex-row list-none md:ml-auto">
						{menuLinks.map((menuLink) => (
							<li key={menuLink.path} className="pl-4 py-2 flex items-center">
								<Link href={`/${menuLink.path}`} className="hover:underline">
									{menuLink.label}
								</Link>
							</li>
						))}
						<li className="pl-4 py-2">
							<DarkModeButton></DarkModeButton>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	);
}
