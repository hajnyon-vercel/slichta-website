"use client";
import { useEffect, useState } from "react";

export function DarkModeButton() {
	const [isDarkModeOn, setIsDarkModeOn] = useState(false);

	const toggleDarkMode = (override?: boolean) => {
		let currentMode: boolean = isDarkModeOn;
		if (override !== undefined) {
			currentMode = !override;
		}
		if (currentMode) {
			localStorage.theme = "light";
			document.documentElement.classList.remove("dark");
		} else {
			localStorage.theme = "dark";
			document.documentElement.classList.add("dark");
		}
		setIsDarkModeOn(!currentMode);
	};

	useEffect(() => {
		if (
			localStorage.theme === "dark" ||
			(!("theme" in localStorage) &&
				window.matchMedia("(prefers-color-scheme: dark)").matches)
		) {
			toggleDarkMode(true);
		} else {
			toggleDarkMode(false);
		}
	});

	return (
		<button
			className="bg-gray-700 rounded p-2 hover:bg-gray-500"
			onClick={(_event) => {
				toggleDarkMode();
			}}
			title="Toggle Dark mode"
		>
			{(isDarkModeOn && `🌞`) || `🌙`}
		</button>
	);
}
