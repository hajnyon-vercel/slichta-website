import Link from "next/link";

import { Container } from "../layout";
import { IAppInfo } from "../../services";

type Props = Readonly<{
	appInfo: IAppInfo;
}>;

export function Footer({ appInfo }: Props) {
	return (
		<footer className="grid-container bg-yellow dark:text-black p-4 mt-8">
			<Container className={"flex justify-between"}>
				<Link href="/" className="hover:underline">
					Šlichta
				</Link>
				<Link href="/docs#changelog" className="hover:underline">
					v{appInfo.version}
				</Link>
			</Container>
		</footer>
	);
}
