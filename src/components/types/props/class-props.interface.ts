import { IProps } from "./props.interface";

export interface IClassProps extends IProps {
	className?: string;
}
