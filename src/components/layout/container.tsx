import { IClassProps } from "../types";

export function Container({ className = "", children }: IClassProps) {
	return <div className={`xl:max-w-screen-xl mx-auto ${className}`}>{children}</div>;
}
