import { Meals } from ".";
import { TimestampToLocal } from "..";
import { IMenu } from "../../services";

interface IMenuProps {
	menu: IMenu;
	onlyToday?: boolean;
}

export function Menu({ menu, onlyToday = false }: IMenuProps) {
	return (
		<>
			{!onlyToday && (
				<h3 className="text-2xl mb-2">
					<TimestampToLocal dateTS={menu.date} />
				</h3>
			)}
			<div className="my-2">
				{menu.data.soups.length > 0 && (
					<Meals meals={menu.data.soups} header={"🍜 Soups"} />
				)}
			</div>
			<div className="my-2">
				{menu.data.meals.length > 0 && (
					<Meals meals={menu.data.meals} header={"🥩 Meals"} />
				)}
			</div>
		</>
	);
}
