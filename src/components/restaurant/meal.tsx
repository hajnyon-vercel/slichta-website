import { Tag } from ".";
import { IMeal } from "../../services";

interface IMealProps {
	meal: IMeal;
}

export function Meal({ meal }: IMealProps) {
	return (
		<div className="flex justify-between p-1 rounded hover:bg-gray-600">
			<div className="flex items-center">
				{meal.name}
				{meal.sideDish && `, ${meal.sideDish}`}
				{meal.description && `, ${meal.description}`}
			</div>
			<div className="flex">
				{meal.tags.length > 0 && (
					<div className="self-center">
						{meal.tags.map((tag, index) => (
							<Tag key={`tag-${index}`} tag={tag} />
						))}
					</div>
				)}
				<div className="self-center bg-yellow p-1 ml-1 rounded whitespace-nowrap dark:text-black">
					{meal.price ?? "-"} Kč
				</div>
			</div>
		</div>
	);
}
