import { ETag } from "../../services/api/types/tag.enum";

interface ITagProps {
	tag: ETag;
}

export function Tag({ tag }: ITagProps) {
	let content = "";
	switch (tag) {
		case ETag.FAST_FOOD:
			content = "🍟";
			break;
		case ETag.DESERT:
			content = "🍰";
			break;
		case ETag.DELIVERY:
			content = "🚗";
			break;
		case ETag.DISCOUNT:
			content = "💰";
			break;
		default:
			content = "❓";
			break;
	}
	return <span title={tag}>{content}</span>;
}
