import { Meal } from ".";
import { IMeal } from "../../services";

interface IMealsProps {
	meals: IMeal[];
	header: string;
}

export function Meals({ meals, header }: IMealsProps) {
	return (
		<>
			<h4 className="text-xl mb-2">{header}</h4>
			<ul>
				{meals.map((meal, index) => (
					<li key={index} className="py-1">
						<Meal meal={meal} />
					</li>
				))}
			</ul>
		</>
	);
}
