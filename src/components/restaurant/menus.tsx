"use client";

import { Menu } from "./menu";
import { IMenu, IRestaurant } from "../../services";

type Props = Readonly<{
	menus: IRestaurant["menus"];
	onlyToday?: boolean;
}>;

const ONE_DAY: number = 1 * 24 * 60 * 60 * 1000;

export function Menus({ menus, onlyToday = false }: Props) {
	const currentDate = new Date();
	currentDate.setHours(0, 0, 0, 0);
	const todayMidnight = currentDate.getTime();

	const isTodayMenu = (menu: IMenu): boolean => {
		return menu.date > todayMidnight && menu.date < todayMidnight + ONE_DAY;
	};

	if (menus.length > 0) {
		const filteredMenus = onlyToday ? menus.filter((menu) => isTodayMenu(menu)) : menus;
		if (filteredMenus.length > 0) {
			return (
				<ul>
					{filteredMenus.map((menu) => (
						<li key={menu.date}>
							<Menu menu={menu} onlyToday={onlyToday} />
						</li>
					))}
				</ul>
			);
		} else {
			return <p>No meals for today. See other days instead.</p>;
		}
	} else {
		return <p>No meals for restaurant.</p>;
	}
}
