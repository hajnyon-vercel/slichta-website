import Link from "next/link";

import { Menus } from "./menus";
import { IRestaurant } from "../../services";

type Props = Readonly<{
	restaurant: IRestaurant;
	onlyToday?: boolean;
}>;

export function Restaurant({ restaurant, onlyToday = false }: Props) {
	return (
		<>
			<Link
				href={`/restaurants/${restaurant.id}`}
				className="border-b-2 mb-4 hover:border-yellow"
			>
				<h2 className="text-4xl">{restaurant.name}</h2>
			</Link>
			<Menus menus={restaurant.menus} onlyToday={onlyToday} />
		</>
	);
}
