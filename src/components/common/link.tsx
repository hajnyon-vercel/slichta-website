import { IClassProps } from '..';

interface ILinkProps extends IClassProps {
    href: string;
    target?: string;
}

export function Link({ children, href, target = '_blank', className = 'underline hover:no-underline' }: ILinkProps) {
    return (
        <a href={href} target={target} className={className}>
            {children}
        </a>
    );
}
