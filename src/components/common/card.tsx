import { IProps } from '..';

export function Card({ children }: IProps) {
    return (
        <div className="w-full justify-center items-center bg-white shadow-lg rounded-lg flex flex-col dark:bg-gray-900 dark:text-white">
            <div className="w-full p-4 justify-start flex flex-col">{children}</div>
        </div>
    );
}
