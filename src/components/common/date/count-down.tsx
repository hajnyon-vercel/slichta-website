"use client";
import { compareAsc, formatDuration, formatISO, intervalToDuration } from "date-fns";
import { useEffect, useState } from "react";

interface IDateProps {
	to: string;
	afterMessage: string;
	className?: string;
}

export function CountDown({ to, afterMessage, className }: IDateProps) {
	let [countDown, setCountDown] = useState<string>();

	const toDate = new Date(to);

	useEffect(() => {
		const updateDuration = () => {
			const currentDate = new Date();
			if (compareAsc(currentDate, toDate) > 0) {
				setCountDown(afterMessage);
			} else {
				let duration = intervalToDuration({
					start: new Date(),
					end: toDate,
				});
				setCountDown(
					formatDuration(duration, {
						delimiter: " & ",
						format: ["days", "hours", "minutes"],
					})
				);
			}
		};
		updateDuration();
		const timer = setInterval(updateDuration, 10000);
		return () => clearInterval(timer);
	});

	return (
		<span className={className} title={`At ${formatISO(toDate)}`}>
			{countDown}
		</span>
	);
}
