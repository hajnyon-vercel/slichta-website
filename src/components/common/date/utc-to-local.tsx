import { format, formatISO, parseISO } from 'date-fns';
import { cs } from 'date-fns/locale';

interface IDateProps {
    dateUTC: string;
    className?: string;
}

export function UTCToLocal({ dateUTC, className }: IDateProps) {
    const date = parseISO(dateUTC);
    return (
        <time dateTime={formatISO(date)} className={className}>
            {format(date, 'd. M. yyyy h:mm', { locale: cs })}
        </time>
    );
}
