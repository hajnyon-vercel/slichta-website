import { format, formatISO, fromUnixTime, millisecondsToSeconds } from 'date-fns';
import { cs } from 'date-fns/locale';

interface IDateProps {
    dateTS: number;
}

export function TimestampToLocal({ dateTS }: IDateProps) {
    const date = fromUnixTime(millisecondsToSeconds(dateTS));
    return <time dateTime={formatISO(date)}>{format(date, 'd. M. yyyy', { locale: cs })}</time>;
}
