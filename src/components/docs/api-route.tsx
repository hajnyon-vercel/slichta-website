import { Link } from "../index";

interface IApiRouteProps {
	method?: string;
	label?: string;
	url: string;
}

export function ApiRoute({ method = "GET", label, url }: IApiRouteProps) {
	return (
		<div className="font-mono text-gray-600 bg-gray-100 border-2 rounded border-gray-100 p-4 my-2 flex justify-between items-center">
			<div className="flex items-center">
				<div className="text-white bg-black border-2 rounded border-black p-1 mr-4">
					{method}
				</div>
				<div>{label ?? url}</div>
			</div>
			<div>
				<Link href={url} target="_blank">try it</Link>
			</div>
		</div>
	);
}
