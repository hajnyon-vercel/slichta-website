"use client";

type Props = Readonly<{
	reset: () => void;
}>;

export default function GlobalError({ reset }: Props) {
	return (
		<html lang="en">
			<body>
				<div className="w-full">
					<h2 className="text-4xl my-4">500 - Internal server error</h2>
					<button onClick={() => reset()}>Try again</button>
				</div>
			</body>
		</html>
	);
}
