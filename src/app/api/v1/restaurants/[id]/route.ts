import { NextRequest, NextResponse } from "next/server";
import { ApifyService, IRestaurant } from "../../../../../services";

export const dynamic = "force-dynamic";

type Params = Readonly<{ params: { id: string } }>;

export async function GET(_request: NextRequest, { params }: Params) {
	const restaurantsData = await ApifyService.getRestaurants();

	if (restaurantsData === null) {
		return NextResponse.json(
			{ error: "No available restaurants.", code: 500 },
			{ status: 500 }
		);
	}

	const result = restaurantsData.find((restaurant: IRestaurant) => restaurant.id === params.id);

	if (result !== undefined) {
		return NextResponse.json(result);
	}

	return NextResponse.json(
		{ error: `No restaurant '${params.id}' was found.`, code: 404 },
		{ status: 404 }
	);
}
