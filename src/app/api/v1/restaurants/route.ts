import { NextRequest, NextResponse } from "next/server";
import { ApifyService, IRestaurant } from "../../../../services";

type IData = Omit<IRestaurant, "menus"> | IRestaurant;

export const dynamic = "force-dynamic";

export async function GET(request: NextRequest) {
	const restaurantsData = await ApifyService.getRestaurants();
	if (restaurantsData === null) {
		return NextResponse.json(
			{ error: "No available restaurants.", code: 500 },
			{ status: 500 }
		);
	}

	const searchParams = request.nextUrl.searchParams;
	const menusQueryParam = searchParams.get("menus");

	let result: IData[] = restaurantsData;
	if (menusQueryParam !== "true") {
		result = restaurantsData.map<IData>((restaurant: IRestaurant): IData => {
			return {
				id: restaurant.id,
				name: restaurant.name,
				url: restaurant.url,
			};
		});
	}

	return NextResponse.json(result, { status: 200 });
}
