import { Metadata } from "next";

import "../styles/index.css";
import { Container, Footer, Header } from "../components";
import { ChangelogService } from "../services";

export const metadata: Metadata = {
	title: {
		default: "Šlichta - restaurant's menus",
		template: "%s | Šlichta",
	},
	description: "Šlichta gathers restaurant's menus into single clear overview.",
	openGraph: {
		title: "Šlichta - restaurant's menus",
		description: "Šlichta gathers restaurant's menus into single clear overview.",
		type: "website",
		url: "https://slichta.vercel.app",
		images: {
			url: "https://slichta.vercel.app/og-cover.png",
			alt: "Šlichta - restaurant's menus",
			width: 984,
			height: 516,
		},
	},
};

type Props = Readonly<{ children: React.ReactNode }>;

export default async function RootLayout({ children }: Props) {
	const appInfo = await ChangelogService.getAppInfo();
	return (
		<html lang="en">
			<head>
				<meta charSet="utf-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				{/* favicons */}
				<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
				<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
				<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
				<link rel="manifest" href="/site.webmanifest" />
				<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
				<meta name="msapplication-TileColor" content="#fbe748" />
				<meta name="theme-color" content="#fbe748" />
			</head>
			<body className="dark:bg-gray-600 dark:text-white">
				<div>
					<Header />
					<Container className="my-4 px-4">{children}</Container>
					<Footer appInfo={appInfo} />
				</div>
			</body>
		</html>
	);
}
