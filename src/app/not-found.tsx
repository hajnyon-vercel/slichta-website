export default function NotFound() {
	return (
		<div className="w-full">
			<h2 className="text-4xl my-4">404 - page not found</h2>
		</div>
	);
}
