import Link from "next/link";

import { Link as BasicLink } from "../../components";

export const metadata = {
	title: "About",
};

export default async function AboutPage() {
	return (
		<div className="w-full">
			<h2 className="text-4xl my-4">About</h2>
			<p>
				This application was created as a successor to{" "}
				<BasicLink href="https://hajnyon.gitlab.io/jcu-menza-scraper/">
					JCU Menza Scraper
				</BasicLink>{" "}
				project, which scraped menus from students cafeteria website and displayed it in a
				more readable format. As me and my fellow colleagues started to dine in other
				restaurants more often, I decided to group all relevant menus to one place. You can
				read a short story in my{" "}
				<BasicLink href="https://blog.hajnyon.cz/scrape-all-you-can-give-nothing-back/#where-will-we-eat-today">
					blog post
				</BasicLink>
				.
			</p>
			<p>
				This project is build using the help of two great tools:{" "}
				<BasicLink href="https://apify.com">Apify</BasicLink> and{" "}
				<BasicLink href="https://nextjs.org">Next.JS</BasicLink>. Schedules Apify&apos;s
				actor scrapes periodically data from websites and provides data to an app hosted on
				Vercel. You can find repositories links bellow.
			</p>
			<h3 className="text-2xl my-2">Resources</h3>
			<ul>
				<li>
					<Link href="/docs#contributing" className="underline hover:no-underline">
						contributing guide
					</Link>
				</li>
				<li>
					<Link href="/docs#changelog" className="underline hover:no-underline">
						changelog
					</Link>
				</li>
				<li>
					<BasicLink
						href="https://gitlab.com/hajnyon-projects/slichta/slichta-website"
						target="_blank"
					>
						website repository
					</BasicLink>
				</li>
				<li>
					<BasicLink
						href="https://gitlab.com/hajnyon-projects/slichta/slichta-scraper"
						target="_blank"
					>
						scraper repository
					</BasicLink>
				</li>
			</ul>
			<h3 className="text-2xl my-2">Credits</h3>
			<ul>
				<li>
					Logo: Puking Cat by{" "}
					<BasicLink href="https://www.instagram.com/terezjirovcova/" target="_blank">
						@terezjirovcova
					</BasicLink>
				</li>
			</ul>
		</div>
	);
}
