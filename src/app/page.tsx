import Link from "next/link";

import { Card, Infobar, Restaurant } from "../components";
import { ApifyService, IRestaurant } from "../services";

const getData = async () => {
	const restaurants = await ApifyService.getRestaurants();
	const scheduleInfo = await ApifyService.getScheduleInfo();

	return {
		restaurants: restaurants?.sort((a, b) => (a.name > b.name ? 1 : -1)),
		scheduleInfo,
	};
};

export default async function HomePage() {
	const data = await getData();
	return (
		<>
			{data.scheduleInfo && (
				<div className="w-full my-4">
					<Infobar data={data.scheduleInfo.data} />
				</div>
			)}
			<div className="w-full grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
				{data.restaurants?.map((restaurant: IRestaurant) => (
					<div key={restaurant.id} className="w-full">
						<Card>
							<Restaurant restaurant={restaurant} onlyToday={true} />
							<div className="flex justify-center">
								<Link
									href={`/restaurants/${restaurant.id}`}
									className="p-2 bg-white border-2 border-yellow rounded text-sm hover:bg-yellow dark:bg-gray-600 dark:hover:bg-yellow dark:hover:text-black"
								>
									More
								</Link>
							</div>
						</Card>
					</div>
				))}
			</div>
		</>
	);
}
