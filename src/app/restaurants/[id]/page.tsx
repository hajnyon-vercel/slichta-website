import { Metadata } from "next";
import { Card, Restaurant } from "../../../components";
import { ApifyService, IRestaurant } from "../../../services";
import { notFound } from "next/navigation";

export async function generateMetadata({ params }: Props): Promise<Metadata> {
	const restaurants = await ApifyService.getRestaurants();
	const restaurant: IRestaurant | undefined = restaurants?.find(
		(restaurant) => restaurant.id === params.id
	);
	return {
		title: restaurant?.name,
	};
}

const getData = async (params: Props["params"]) => {
	const restaurants = await ApifyService.getRestaurants();

	const restaurant: IRestaurant | undefined = restaurants?.find(
		(restaurant) => restaurant.id === params.id
	);
	if (!restaurant) {
		notFound();
	}

	return {
		restaurant,
	};
};

type Props = Readonly<{
	params: {
		id?: string;
	};
}>;

export default async function RestaurantPage({ params }: Props) {
	const data = await getData(params);

	return (
		<div className="w-full">
			<div key={data.restaurant.id} className="w-full">
				<Card>
					<Restaurant restaurant={data.restaurant} onlyToday={false} />
				</Card>
			</div>
		</div>
	);
}
