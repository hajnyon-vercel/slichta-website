import { ApiRoute, Infobar, Link } from "../../components";
import { ApifyService, ChangelogService, mdToHtml } from "../../services";

export const metadata = {
	title: "Docs",
};

const getData = async () => {
	const changelog = await ChangelogService.getChangelog();
	const changelogFormatted = await mdToHtml(changelog);
	const scheduleInfo = await ApifyService.getScheduleInfo();

	return {
		changelog: changelogFormatted.toString(),
		scheduleInfo,
	};
};

export default async function DocsPage() {
	const data = await getData();
	const baseRepositoryUrl = "https://gitlab.com/hajnyon-projects/slichta/slichta-website/-/blob/main";
	return (
		<div className="w-full">
			<h2 className="text-4xl my-8">Docs</h2>

			<h3 className="text-2xl my-6 border-b-2">Schedule</h3>
			{data.scheduleInfo && (
				<>
					<p>
						Šlichta&apos;s scraper is set to run periodically using{" "}
						<Link href="https://docs.apify.com/schedules">Apify&apos;s Schedules</Link>.
						It&apos;s cron is set to{" "}
						<Link
							href={`https://crontab.guru/#${encodeURIComponent(
								data.scheduleInfo.data.cronExpression
							)}`}
							className="font-mono underline hover:no-underline"
						>
							{data.scheduleInfo.data.cronExpression}
						</Link>{" "}
						which tries to satisfy most users (each restaurant publishes menu at
						different time). You can find latest info below.
					</p>
					<div className="w-full my-4">
						<Infobar data={data.scheduleInfo.data} />
					</div>
				</>
			)}
			{!data.scheduleInfo && (
				<p>There was a problem fetching data about schedule settings.</p>
			)}

			<h3 className="text-2xl my-6 border-b-2">Endpoints</h3>
			<p>Šlichta provides an API access to restaurants menus. See endpoints bellow.</p>

			<h4 className="text-xl my-4">Restaurants</h4>
			<p className="my-2">
				Returns an array of restaurants with basic info. Supports query parameter for
				enabling menus. For data format see{" "}
				<Link href={`${baseRepositoryUrl}/src/services/api/types/restaurant.interface.ts`}> 
					interface
				</Link>
				.
			</p>
			<ApiRoute url={`/api/v1/restaurants`} />
			<ApiRoute url={`/api/v1/restaurants?menus=true`} />

			<h4 className="text-xl my-4">Restaurant by id</h4>
			<p className="my-2">
				Returns single restaurant&apos;s data. For data format see{" "}
				<Link href={`${baseRepositoryUrl}/src/services/api/types/restaurant.interface.ts`}>
					interface
				</Link>
				.
			</p>
			<ApiRoute url={`/api/v1/restaurants/u-flicku`} label={`/api/v1/restaurants/:id`} />

			<h3 id="contributing" className="text-2xl my-6 border-b-2">
				Contributing
			</h3>
			<p>
				If you want to see your favorite restaurant&apos;s menu here you have two options.
				You can either see{" "}
				<Link href={`${baseRepositoryUrl}/CONTRIBUTING.md`}>
					CONTRIBUTING.md
				</Link>{" "}
				and add it yourself (you should have some programming skills) or you can try to
				contact me at{" "}
				<Link href="mailto:hajnyon+slichta@gmail.com">hajnyon+slichta@gmail.com</Link> and
				maybe we can make a deal.
			</p>

			<div className="changelog" dangerouslySetInnerHTML={{ __html: data.changelog }}></div>
		</div>
	);
}
