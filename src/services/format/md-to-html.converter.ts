import Showdown from 'showdown';

const converter: Showdown.Converter = new Showdown.Converter({ openLinksInNewWindow: true, headerLevelStart: 3 });

export async function mdToHtml(markdown: string): Promise<string> {
    const result: string = converter.makeHtml(markdown);
    return result.toString();
}
