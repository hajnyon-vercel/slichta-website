import { promises as fs } from "fs";
import path from "path";

import { IAppInfo } from "./app-info.interface";

export class ChangelogService {
	public static async getCurrentVersion(): Promise<string> {
		const packagePath = path.join(process.cwd(), "package.json");
		const packageJson = await fs.readFile(packagePath, { encoding: "utf-8" });
		const packageJsonParsed = JSON.parse(packageJson);
		return packageJsonParsed.version;
	}

	public static async getChangelog(): Promise<string> {
		const changelogPath = path.join(process.cwd(), "CHANGELOG.md");
		const changelog = await fs.readFile(changelogPath, { encoding: "utf-8" });
		return changelog;
	}

	public static async getAppInfo(): Promise<IAppInfo> {
		const version = await ChangelogService.getCurrentVersion();
		return { version };
	}
}
