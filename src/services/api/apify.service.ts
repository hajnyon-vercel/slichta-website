import { IRestaurant, IScheduleInfo } from "./types";

const API_URL = process.env.APIFY_API_URL;
const API_TOKEN = process.env.APIFY_API_TOKEN;

async function getJson(path: string): Promise<any> {
	const headers = { "Content-Type": "application/json" };

	const url: string = `${API_URL}/${path}?token=${API_TOKEN}`;

	const result = await fetch(url, {
		method: "GET",
		headers,
		cache: "no-cache",
	});

	const json = await result.json();
	return json;
}

export class ApifyService {
	public static async getRestaurants(): Promise<IRestaurant[] | null> {
		try {
			const data: IRestaurant[] = await getJson(
				`actor-tasks/hajnyon~slichta-scraper-task/runs/last/dataset/items`
			);
			return data;
		} catch (error) {
			console.error(error);
			return null;
		}
	}

	public static async getScheduleInfo(): Promise<IScheduleInfo | null> {
		try {
			const data: IScheduleInfo = await getJson(`schedules/dhltBEORpgQW2Byqa`);
			return data;
		} catch (error) {
			console.error(error);
			return null;
		}
	}
}
