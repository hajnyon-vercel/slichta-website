export interface IScheduleInfo {
    data: {
        id: string;
        userId: string;
        name: string;
        cronExpression: string;
        timezone: string;
        isEnabled: boolean;
        isExclusive: boolean;
        createdAt: string;
        modifiedAt: string;
        nextRunAt: string;
        lastRunAt: string;
        actions: [
            {
                id: string;
                type: string;
                actorTaskId: string;
                input: {
                    [key: string]: unknown;
                };
                createdAt: string;
            }
        ];
    };
}
