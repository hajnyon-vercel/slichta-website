import { ETag } from './tag.enum';

export interface IMeal {
    /**
     * Meal name (whole meal information if meal parts can't be parsed).
     */
    readonly name: string;
    /**
     * Price in czech crowns.
     */
    readonly price: number | null;
    /**
     * Special meal tags (eg. 'desert' or 'discount').
     */
    readonly tags: ETag[];
    /**
     * Weight of meal if available.
     */
    readonly weight?: number;
    /**
     * Side dish if can be parsed.
     */
    readonly sideDish?: string;
    /**
     * Meal description if can be parsed.
     */
    readonly description?: string;
}
