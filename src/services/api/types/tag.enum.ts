export enum ETag {
    FAST_FOOD = 'fastFood',
    DELIVERY = 'delivery',
    DESERT = 'desert',
    DISCOUNT = 'discount'
}
