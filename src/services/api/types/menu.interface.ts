import { IMeal } from './meal.interface';

export interface IMenu {
    readonly date: number;
    readonly data: {
        readonly soups: IMeal[];
        readonly meals: IMeal[];
    };
}
