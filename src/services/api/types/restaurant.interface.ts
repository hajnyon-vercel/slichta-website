import { IMenu } from './menu.interface';

export interface IRestaurant {
    readonly id: string;
    readonly url: string;
    readonly name: string;
    readonly menus: IMenu[];
}
