export interface IErrorResponse {
    error: string;
    code: number;
}
