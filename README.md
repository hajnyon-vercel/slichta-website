# slichta-website

Website displaying various restaurants daily menus. It is built with [nextjs](https://nextjs.org/) on data scraped with [slichta-scraper](https://gitlab.com/hajnyon-projects/slichta/slichta-scraper) using [Apify](https://apify.com/).

## 🏗️ Development

Before you start you need to set up `.env.local` (copy `.env.local.example`) and fill in Apify url and token.

```bash
npm ci
npm run dev
```

## 🚀 Deployment

### Locally

```bash
npm i -g vercel
vercel login
vercel build
vercel deploy
```

### CI/CD

- Vercel's and project's environmental variables must be set
  - for Vercel see [guide](https://vercel.com/guides/how-can-i-use-gitlab-pipelines-with-vercel#deploying-your-vercel-application-with-gitlab-ci/cd)
  - for project see `.env.local.example`
- example below is for production environment (for preview see `.gitlab-ci.yml`)

```bash
npm install --global vercel
vercel pull --yes --environment=production --token=$VERCEL_TOKEN
vercel build --prod --token=$VERCEL_TOKEN
vercel deploy --prebuilt --prod --token=$VERCEL_TOKEN
```
